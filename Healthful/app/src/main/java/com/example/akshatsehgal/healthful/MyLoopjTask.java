package com.example.akshatsehgal.healthful;
import android.content.Context;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
/**
 * Created by Akshat Sehgal on 11/5/2016.
 */
public class MyLoopjTask{

    private static final String TAG = "AKSHAT";
    private OnLoopjCompleted listener;
    private Context context;

    AsyncHttpClient asyncHttpClient;
    RequestParams requestParams;

    String BASE_URL = "http://healthfulwebapp-1.4j6hmqcmpj.us-west-2.elasticbeanstalk.com/healthful/driver";
    String jsonResponse;

    public MyLoopjTask(Context context, OnLoopjCompleted listener) {
        Log.d(TAG, "inside MyLoopjTask constructor");
        asyncHttpClient = new AsyncHttpClient();
        requestParams = new RequestParams();
        this.context = context;
        this.listener = listener;
    }

    public void executeLoopjCall(String queryTerm) {
        Log.d(TAG, "executeLoopjCall");
        requestParams.put("s",queryTerm);
        asyncHttpClient.get(BASE_URL, null, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                jsonResponse = response.toString();
                listener.taskCompleted(jsonResponse);
                Log.d(TAG, "onSuccess: " + jsonResponse);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.d(TAG, "onFailure: " + errorResponse);
            }
        });
    }
}
