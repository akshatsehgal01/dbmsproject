package com.healthful.restjersey;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/driver")
public class driver {
	@GET
	@Produces("application/json")
	public String healthfulDriver() {
		String teamMembers = "";
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			System.out.println("Checking Connection!!");
			Connection con = DriverManager.getConnection(
					"jdbc:oracle:thin:@healthful.cxqy06ucx5dg.us-west-2.rds.amazonaws.com:1521:orcl", "Healthful",
					"rajni12345");
			PreparedStatement statement = con
					.prepareStatement("SELECT * FROM HEALTHFULTEAM WHERE SNO = (SELECT MAX(SNO) FROM HEALTHFULTEAM)");
			ResultSet result = statement.executeQuery();
			System.out.println("Checking Stage!");

			while (result.next()) {
				teamMembers = teamMembers + result.getString("SNO") + result.getString("name");
			}
			System.out.println("while ke pehle"+teamMembers);
		} catch (SQLException exc) {
			System.out.println(exc.getMessage());
		} catch (ClassNotFoundException exc) {
			System.out.println(exc.getMessage());
		}
		System.out.println("while ke baad"+teamMembers);
		//return teamMembers;
		//return  ( "<check>" + teamMembers + "</check>") ;
		return HealthfulUtility.constructJSON(teamMembers);
	}
}
