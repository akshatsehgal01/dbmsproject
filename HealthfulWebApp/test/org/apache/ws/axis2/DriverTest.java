

/**
 * DriverTest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.4  Built on : Oct 21, 2016 (10:47:34 BST)
 */
    package org.apache.ws.axis2;

    /*
     *  DriverTest Junit test case
    */

    public class DriverTest extends junit.framework.TestCase{

     
        /**
         * Auto generated test method
         */
        public  void testhealthfulDriver() throws java.lang.Exception{

        org.apache.ws.axis2.DriverStub stub =
                    new org.apache.ws.axis2.DriverStub();//the default implementation should point to the right endpoint

           org.apache.ws.axis2.DriverStub.HealthfulDriver healthfulDriver4=
                                                        (org.apache.ws.axis2.DriverStub.HealthfulDriver)getTestObject(org.apache.ws.axis2.DriverStub.HealthfulDriver.class);
                    // TODO : Fill in the healthfulDriver4 here
                
                        assertNotNull(stub.healthfulDriver(
                        healthfulDriver4));
                  



        }
        
         /**
         * Auto generated test method
         */
        public  void testStarthealthfulDriver() throws java.lang.Exception{
            org.apache.ws.axis2.DriverStub stub = new org.apache.ws.axis2.DriverStub();
             org.apache.ws.axis2.DriverStub.HealthfulDriver healthfulDriver4=
                                                        (org.apache.ws.axis2.DriverStub.HealthfulDriver)getTestObject(org.apache.ws.axis2.DriverStub.HealthfulDriver.class);
                    // TODO : Fill in the healthfulDriver4 here
                

                stub.starthealthfulDriver(
                         healthfulDriver4,
                    new tempCallbackN65548()
                );
              


        }

        private class tempCallbackN65548  extends org.apache.ws.axis2.DriverCallbackHandler{
            public tempCallbackN65548(){ super(null);}

            public void receiveResulthealthfulDriver(
                         org.apache.ws.axis2.DriverStub.HealthfulDriverResponse result
                            ) {
                
            }

            public void receiveErrorhealthfulDriver(java.lang.Exception e) {
                fail();
            }

        }
      
        //Create an ADBBean and provide it as the test object
        public org.apache.axis2.databinding.ADBBean getTestObject(java.lang.Class type) throws java.lang.Exception{
           return (org.apache.axis2.databinding.ADBBean) type.newInstance();
        }

        
        

    }
    